package com.example.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MsProductApplicationTests {

	productController testobj;
    productDetails ob;
    List<productDetails> list;
    @Before
    public void init() {
        testobj= new productController();
        ob=new productDetails();
        list=new ArrayList<>();
        String[] image=new String[2];
        image[0]="abc";
        image[1]="def";
        String[] store=new String[2];
        store[0]="123";
        store[1]="456";
        list.add(new productDetails("1","Dettol",50,"soap","Dettol",9.8f,"good",image,store));
        String[] image1=new String[2];
        image1[0]="cloth.com";
        image1[1]="abc.com";
        String[] store1=new String[2];
        store1[0]="789";
        store1[1]="012";
        list.add(new productDetails("2","Monte",2500,"cloth","Carlo",9.6f,"great",image1,store1));
    }
	
	@Test
	public void testgetProductsSize() {
		 final String URL_PRODUCT = "http://localhost:8099/getProducts";
	     HttpHeaders headers = new HttpHeaders();
	     headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
	     headers.setContentType(MediaType.APPLICATION_JSON);
	    HttpEntity<productDetails[]> entity = new HttpEntity<productDetails[]>(headers);
	    RestTemplate restTemplate = new RestTemplate();
	    ResponseEntity<productDetails[]> response = restTemplate.exchange(URL_PRODUCT,HttpMethod.GET, entity, productDetails[].class);
	    productDetails[] result = response.getBody();
	    List<productDetails> asresult=Arrays.asList(result);
//	    asresult.stream().forEach(System.out::println);
//	    list.stream().forEach(System.out::println);
	    assertEquals(list.size(),asresult.size());
	}
	
	@Test
	public void testgetProductsSizeCheck() {
		 final String URL_PRODUCT = "http://localhost:8099/getProducts";
	     HttpHeaders headers = new HttpHeaders();
	     headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
	     headers.setContentType(MediaType.APPLICATION_JSON);
	    HttpEntity<productDetails[]> entity = new HttpEntity<productDetails[]>(headers);
	    RestTemplate restTemplate = new RestTemplate();
	    ResponseEntity<productDetails[]> response = restTemplate.exchange(URL_PRODUCT,HttpMethod.GET, entity, productDetails[].class);
	    productDetails[] result = response.getBody();
	    List<productDetails> asresult=Arrays.asList(result);
//	    asresult.stream().forEach(System.out::println);
//	    list.stream().forEach(System.out::println);
	    assertNotEquals(0,asresult.size());
	}
	
	@Test
	public void testgetProductsSizeCheckExp() {
		 final String URL_PRODUCT = "http://localhost:8099/getProducts";
	     HttpHeaders headers = new HttpHeaders();
	     headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
	     headers.setContentType(MediaType.APPLICATION_JSON);
	    HttpEntity<productDetails[]> entity = new HttpEntity<productDetails[]>(headers);
	    RestTemplate restTemplate = new RestTemplate();
	    ResponseEntity<productDetails[]> response = restTemplate.exchange(URL_PRODUCT,HttpMethod.GET, entity, productDetails[].class);
	    productDetails[] result = response.getBody();
	    List<productDetails> asresult=Arrays.asList(result);
//	    asresult.stream().forEach(System.out::println);
//	    list.stream().forEach(System.out::println);
	    assertNotEquals("null",asresult);
	}
	
	@Test
	public void testgetProducts() {
		 final String URL_PRODUCT = "http://localhost:8099/getProducts";
	     HttpHeaders headers = new HttpHeaders();
	     headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
	     headers.setContentType(MediaType.APPLICATION_JSON);
	    HttpEntity<productDetails[]> entity = new HttpEntity<productDetails[]>(headers);
	    RestTemplate restTemplate = new RestTemplate();
	    ResponseEntity<String> response = restTemplate.exchange(URL_PRODUCT,HttpMethod.GET, entity, String.class);
	    String result = response.getBody();
	    //System.out.println(result);
	    String test = "[{\"name\":\"Dettol\",\"price\":50,\"category\":\"soap\",\"companyName\":\"Dettol\",\"rating\":0.0,\"description\":\"good\",\"imageUrls\":[\"abc\",\"def\"],\"storeId\":[\"123\",\"456\"],\"id\":\"1\"},{\"name\":\"Monte\",\"price\":2500,\"category\":\"cloth\",\"companyName\":\"Carlo\",\"rating\":9.6,\"description\":\"great\",\"imageUrls\":[\"cloth.com\",\"abc.com\"],\"storeId\":[\"789\",\"012\"],\"id\":\"2\"}]";
	    //System.out.println(test);
	    assertEquals(test,result);
	}
	
	@Test
	public void testgetProductsCheck() {
		 final String URL_PRODUCT = "http://localhost:8099/getProducts";
	     HttpHeaders headers = new HttpHeaders();
	     headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
	     headers.setContentType(MediaType.APPLICATION_JSON);
	    HttpEntity<productDetails[]> entity = new HttpEntity<productDetails[]>(headers);
	    RestTemplate restTemplate = new RestTemplate();
	    ResponseEntity<String> response = restTemplate.exchange(URL_PRODUCT,HttpMethod.GET, entity, String.class);
	    String result = response.getBody();
	    //System.out.println(result);
	    String test = "{\"name\":\"Dettol\",\"price\":50,\"category\":\"soap\",\"companyName\":\"Dettol\",\"rating\":0.0,\"description\":\"good\",\"imageUrls\":[\"abc\",\"def\"],\"storeId\":[\"123\",\"456\"],\"id\":\"1\"},{\"name\":\"Monte\",\"price\":2500,\"category\":\"cloth\",\"companyName\":\"Carlo\",\"rating\":9.6,\"description\":\"great\",\"imageUrls\":[\"cloth.com\",\"abc.com\"],\"storeId\":[\"789\",\"012\"],\"id\":\"2\"}";
	    //System.out.println(test);
	    assertNotEquals(test,result);
	}
	
	@Test
	public void testgetProductsCheckExp() {
		 final String URL_PRODUCT = "http://localhost:8099/getProducts";
	     HttpHeaders headers = new HttpHeaders();
	     headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
	     headers.setContentType(MediaType.APPLICATION_JSON);
	    HttpEntity<productDetails[]> entity = new HttpEntity<productDetails[]>(headers);
	    RestTemplate restTemplate = new RestTemplate();
	    ResponseEntity<String> response = restTemplate.exchange(URL_PRODUCT,HttpMethod.GET, entity, String.class);
	    String result = response.getBody();
	    //System.out.println(result);
	    //System.out.println(test);
	    assertNotEquals("null",result);
	}
	
	@Test
	public void testgetProductById() {
		final String URL_PRODUCT = "http://localhost:8099/getProductbyId/1";
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    HttpEntity<Optional<productDetails>> entity = new HttpEntity<Optional<productDetails>>(headers);
	    RestTemplate restTemplate = new RestTemplate();
	    ResponseEntity<String> response = restTemplate.exchange(URL_PRODUCT,HttpMethod.GET, entity, String.class);
	    String result = response.getBody();
	    String test = "{\"name\":\"Dettol\",\"price\":50,\"category\":\"soap\",\"companyName\":\"Dettol\",\"rating\":0.0,\"description\":\"good\",\"imageUrls\":[\"abc\",\"def\"],\"storeId\":[\"123\",\"456\"],\"id\":\"1\"}";
	    //System.out.println(test);
	    //System.out.println(result);
	    assertEquals(test,result);
	}
	
	@Test
	public void testgetProductByIdExp() {
		final String URL_PRODUCT = "http://localhost:8099/getProductbyId/3";
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    HttpEntity<Optional<productDetails>> entity = new HttpEntity<Optional<productDetails>>(headers);
	    RestTemplate restTemplate = new RestTemplate();
	    ResponseEntity<String> response = restTemplate.exchange(URL_PRODUCT,HttpMethod.GET, entity, String.class);
	    String result = response.getBody();
	    System.out.println(result);
	    assertEquals("null",result);
	}
	
	@Test
	public void testgetProductByIdCheck() {
		final String URL_PRODUCT = "http://localhost:8099/getProductbyId/1";
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    HttpEntity<Optional<productDetails>> entity = new HttpEntity<Optional<productDetails>>(headers);
	    RestTemplate restTemplate = new RestTemplate();
	    ResponseEntity<String> response = restTemplate.exchange(URL_PRODUCT,HttpMethod.GET, entity, String.class);
	    String result = response.getBody();
	    String test = "\"name\":\"Dettol\",\"price\":50,\"category\":\"soap\",\"companyName\":\"Dettol\",\"rating\":0.0,\"description\":\"good\",\"imageUrls\":[\"abc\",\"def\"],\"storeId\":[\"123\",\"456\"],\"id\":\"1\"";
	    //System.out.println(test);
	    //System.out.println(result);
	    assertNotEquals(test,result);
	}
	
	/*@Test
	public void testdeleteProduct() {
		final String URL_PRODUCT = "http://localhost:8099/deleteProduct/5d5e20a9fbb5785a80eecedd";
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    HttpEntity<Optional<productDetails>> entity = new HttpEntity<Optional<productDetails>>(headers);
	    RestTemplate restTemplate = new RestTemplate();
	    ResponseEntity<String> response = restTemplate.exchange(URL_PRODUCT,HttpMethod.GET, entity, String.class);
	    String result = response.getBody();
	    System.out.println(result);
	    assertEquals("Deleted",result);
	}*/

	@Test
	public void testgetProductByName() {
		final String URL_PRODUCT = "http://localhost:8099/getProductbyName/det";
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    HttpEntity<Optional<productDetails>> entity = new HttpEntity<Optional<productDetails>>(headers);
	    RestTemplate restTemplate = new RestTemplate();
	    ResponseEntity<String> response = restTemplate.exchange(URL_PRODUCT,HttpMethod.GET, entity, String.class);
	    String result = response.getBody();
	    String test = "[{\"name\":\"Dettol\",\"price\":50,\"category\":\"soap\",\"companyName\":\"Dettol\",\"rating\":0.0,\"description\":\"good\",\"imageUrls\":[\"abc\",\"def\"],\"storeId\":[\"123\",\"456\"],\"id\":\"1\"}]";
	    //System.out.println(test);
	    //System.out.println(result);
	    assertEquals(test,result);
	}
	
	@Test
	public void testgetProductByNameCheck() {
		final String URL_PRODUCT = "http://localhost:8099/getProductbyName/det";
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    HttpEntity<Optional<productDetails>> entity = new HttpEntity<Optional<productDetails>>(headers);
	    RestTemplate restTemplate = new RestTemplate();
	    ResponseEntity<String> response = restTemplate.exchange(URL_PRODUCT,HttpMethod.GET, entity, String.class);
	    String result = response.getBody();
	    String test = "{\"name\":\"Dettol\",\"price\":50,\"category\":\"soap\",\"companyName\":\"Dettol\",\"rating\":0.0,\"description\":\"good\",\"imageUrls\":[\"abc\",\"def\"],\"storeId\":[\"123\",\"456\"],\"id\":\"1\"}";
	    //System.out.println(test);
	    //System.out.println(result);
	    assertNotEquals(test,result);
	}
	
	@Test
	public void testgetProductByNameCheckExp() {
		final String URL_PRODUCT = "http://localhost:8099/getProductbyName/determinant";
	    HttpHeaders headers = new HttpHeaders();
	    headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    HttpEntity<Optional<productDetails>> entity = new HttpEntity<Optional<productDetails>>(headers);
	    RestTemplate restTemplate = new RestTemplate();
	    ResponseEntity<String> response = restTemplate.exchange(URL_PRODUCT,HttpMethod.GET, entity, String.class);
	    String result = response.getBody();
	    //System.out.println(test);
	    //System.out.println(result);
	    assertNotEquals("null",result);
	}
	
}
