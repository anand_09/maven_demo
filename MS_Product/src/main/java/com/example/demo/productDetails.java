package com.example.demo;


import java.util.Arrays;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "productDetails")
public class productDetails {
	@Id
	private String _id;
	private String name;
	private int price;
	private String category;
	private String companyName;
	private float rating;
	private String description;
	private String[] imageUrls;
	private String[] storeId;
	public String getId() {
		return _id;
	}
	public void setId(String id) {
		this._id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String[] getImageUrls() {
		return imageUrls;
	}
	public void setImageUrls(String[] imageUrls) {
		this.imageUrls = imageUrls;
	}
	public String[] getStoreId() {
		return storeId;
	}
	public void setStoreId(String[] storeId) {
		this.storeId = storeId;
	}
	@Override
	public String toString() {
		return "productDetails [id=" + _id + ", name=" + name + ", price=" + price + ", category=" + category
				+ ", companyName=" + companyName + ", rating=" + rating + ", description=" + description
				+ ", imageUrls=" + Arrays.toString(imageUrls) + ", storeId=" + Arrays.toString(storeId) + "]";
	}
	public productDetails(String id, String name, int price, String category, String companyName, float rating,
			String description, String[] imageUrls, String[] storeId) {
		super();
		this._id = id;
		this.name = name;
		this.price = price;
		this.category = category;
		this.companyName = companyName;
		this.rating = rating;
		this.description = description;
		this.imageUrls = imageUrls;
		this.storeId = storeId;
	}
	public productDetails() {
		super();
	}
	
}
