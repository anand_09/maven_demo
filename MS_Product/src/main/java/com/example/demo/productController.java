package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class productController {
	
	
	
	@Autowired
	MongoOperations mongoOperations;
	@Autowired
	private productRepository repository;
	
	@PostMapping("/addProduct")
	public String addProduct(@RequestBody productDetails pd) {
		repository.save(pd);
		return "Successfully Added";
	}
	
	@GetMapping("/getProducts")
	public List<productDetails> getProducts(){
		List<productDetails> result = repository.findAll();
		try {
			if(result.size()==0)
				throw new ProductNotFoundException("No such product exists!");
			else
				return result;
		}catch (ProductNotFoundException e) {
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	@GetMapping("/getProductbyId/{id}")
	public Optional<productDetails> getProductbyId(@PathVariable("id") String id) {
		Optional<productDetails> result = repository.findById(id);
		try {
			if(result==null)
				throw new ProductNotFoundException("No such product exists!");
			else
				return result;
		}catch (ProductNotFoundException e) {
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	@DeleteMapping("/deleteProduct/{id}")
	public String deleteProduct(@PathVariable("id") String id) {
		if(repository.existsById(id)) {
			repository.deleteById(id);
			return "Deleted";
		}
		else {
			return "Document not found";
		}
	}
	
	@GetMapping("/getProductbyName/{pname}")
	public List<productDetails> getProductbyName(@PathVariable("pname") String pname) {
		Query query = new Query();
		query.addCriteria(Criteria.where("name").regex(pname,"i"));
		List<productDetails> result = mongoOperations.find(query,productDetails.class);
		try {
			if(result.size()==0)
				throw new ProductNotFoundException("No such product exists!");
			else
				return result;
		}catch (ProductNotFoundException e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

}
