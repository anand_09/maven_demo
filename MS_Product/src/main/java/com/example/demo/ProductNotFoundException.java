package com.example.demo;

public class ProductNotFoundException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ProductNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * This exception is thrown when a valid product doesn't exists. 
	 */
	
	/**
	 * 
	 * @param message The message to be printed 
	 */
	public ProductNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

}
