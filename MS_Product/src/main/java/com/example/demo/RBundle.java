package com.example.demo;

import java.util.ResourceBundle;

public class RBundle {
   
    private RBundle() {
        super();
    }

    public static String getValues(String key)
    {
        ResourceBundle rb = ResourceBundle.getBundle("oms");
        return rb.getString(key);
    }
}