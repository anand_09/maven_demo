package Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class StudentDAO {
	private static int noUser=1;
	
	public static ArrayList<StudentBean> getStudents() throws Exception {
		Connection co = Dbconn.getMyConnection();
		PreparedStatement ps = co.prepareStatement("select regid,name,Pmarks,Cmarks,Mmarks,percent from student");
		ResultSet rs = ps.executeQuery(); 
		ArrayList<StudentBean> list = new ArrayList<StudentBean>();    
		StudentBean student = null;    
		while(rs.next()) {          
			student = new StudentBean(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4),rs.getInt(5),rs.getFloat(6));    
			list.add(student);
		} 
		return list;
	}
	
	public static ArrayList<StudentBean> getAnyStudent(int id) throws Exception {
		Connection co = Dbconn.getMyConnection();
		PreparedStatement ps = co.prepareStatement("select regid,name,Pmarks,Cmarks,Mmarks,percent from student where REGID = ?");
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery(); 
		ArrayList<StudentBean> list = new ArrayList<>();    
		StudentBean student = null;    
		while(rs.next()) {          
			student = new StudentBean(rs.getInt(1),rs.getString(2),rs.getInt(3),rs.getInt(4),rs.getInt(5),rs.getFloat(6));    
			list.add(student);
		}    
		return list;
	}
	
	public static void insertStudent(StudentBean sb) throws Exception {
		Connection co = Dbconn.getMyConnection();
		PreparedStatement ps = co.prepareStatement("INSERT INTO STUDENT VALUES(?,?,?,?,?,?)");
		ps.setInt(1, sb.getRegid());
		ps.setString(2, sb.getName());
		ps.setInt(3, sb.getPmarks());
		ps.setInt(4, sb.getCmarks());
		ps.setInt(5, sb.getMmarks());
		ps.setFloat(6, sb.getPercent());
		int rs = ps.executeUpdate(); 
		System.out.println(rs+" Players Added");
	}
	
	public static int deleteStudent(int id) throws Exception {
		Connection co = Dbconn.getMyConnection();
		PreparedStatement ps = co.prepareStatement("DELETE FROM STUDENT WHERE ID = ?");
		ps.setInt(1, id);
		int rs = ps.executeUpdate(); 
		return rs;
	}
	
	public static int updateStudent(int id, StudentBean sb) throws Exception {
		Connection co = Dbconn.getMyConnection();
		PreparedStatement ps = co.prepareStatement("UPDATE STUDENT SET NAME=?,PMARKS=?,CMARKS=?,MMARKS=?,PERCENT=? WHERE REGID=?");
		ps.setString(1, sb.getName());
		ps.setInt(2,sb.getPmarks());
		ps.setInt(3,sb.getCmarks());
		ps.setInt(4,sb.getMmarks());
		ps.setFloat(5,sb.getPercent());
		ps.setInt(6,id);
		int rs = ps.executeUpdate(); 
		return rs;
	}
	
	public static int insertUser(String name, String password, String type) throws Exception {
		Connection co = Dbconn.getMyConnection();
		PreparedStatement ps = co.prepareStatement("INSERT INTO LOGIN VALUES(?,?,?,?)");
		ps.setInt(1, noUser);
		noUser++;
		ps.setString(2, type);
		ps.setString(3, name);
		ps.setString(4, password);
		int rs = ps.executeUpdate(); 
		return rs;
	}
	
	public static boolean checkUser(String name, String pass, String type) throws Exception {
		Connection co = Dbconn.getMyConnection();
		PreparedStatement ps = co.prepareStatement("SELECT REGID FROM LOGIN WHERE  PASSWORD=? and NAME=? and TYPE=?");
		ps.setString(1, pass);
		ps.setString(2, name);
		ps.setString(3, type);
		ResultSet rs = ps.executeQuery(); 
		if(rs.next())
			return true;
		return false;
	}

}
