package Project;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Registration
 */
public class Registration extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Registration() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out=response.getWriter();  
		 try {    
			 out.print("<html><center><title>Registration Page</title><body><h1>Hey, Welcome to Registration!!!</h1>"); 
			 out.print("<form action='' method='post' >");
			 out.print("<p>Please select your position :</p>");
			 out.print("<input type='radio' name='type' value='student'/>Student<br>");
			 out.print("<input type='radio' name='type' value='admin'/>Admin<br>");
			 out.print("<br>Enter username <input type= 'text' value='null' name='a1'/>");    
			 out.print("<br>Enter password <input type= 'password' name='a2'/>");   
			 out.print("<br><input type= 'SUBMIT' name='login' value='LOGIN'/>");     
			 out.print("<br><input type= 'SUBMIT' name='login' value='REGISTER'/>");
			 
			 
			 String log = request.getParameter("login");
			 if(log.equalsIgnoreCase("LOGIN")) {
					RequestDispatcher rd = request.getRequestDispatcher("Login)");
					rd.forward(request,response);
				}
			 
			 String type = request.getParameter("type");
			 String name = request.getParameter("a1"); 
			 String pass = request.getParameter("a2"); 
			 
			 if(log.equalsIgnoreCase("register")) {
					if(type.equalsIgnoreCase("admin")) {
						StudentDAO.insertUser(name, pass, type);
						out.print("You are registered as an Admin");
					 }
					 else if(type.equalsIgnoreCase("student")) {
						StudentDAO.insertUser(name, pass, type);
						out.print("You are registered as a Student");
					 }
					out.print("Now, You can log in");
					RequestDispatcher rd = request.getRequestDispatcher("Login");
					rd.forward(request,response);
				}
			 out.print("</form></body></center></html>");
			}  
		 catch(Exception e) { 
			 out.print("rrrrrNot found"+e);
		 }
	}

}
