package Project;

public class StudentBean {
	private String name;
	private int regid;
	private int Pmarks;
	private int Cmarks;
	private int Mmarks;
	private float percent;
	
	public StudentBean() {
		super();
	}
	
	public StudentBean(int int1, String string, int int2, int int3, int int4, float float1) {
		super();
		this.regid=int1;
		this.name=string;
		this.Pmarks=int2;
		this.Cmarks=int3;
		this.Mmarks=int4;
		this.percent=float1;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getRegid() {
		return regid;
	}
	public void setRegid(int regid) {
		this.regid = regid;
	}
	public int getPmarks() {
		return Pmarks;
	}
	public void setPmarks(int pmarks) {
		Pmarks = pmarks;
	}
	public int getCmarks() {
		return Cmarks;
	}
	public void setCmarks(int cmarks) {
		Cmarks = cmarks;
	}
	public int getMmarks() {
		return Mmarks;
	}
	public void setMmarks(int mmarks) {
		Mmarks = mmarks;
	}
	public float getPercent() {
		return percent;
	}
	public void setPercent(float percent) {
		this.percent = percent;
	}
	

}
