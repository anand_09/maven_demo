package Project;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdminHomepage
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out=response.getWriter();  
		 try {    
			 out.print("<html><center><title>Student Project</title><body><h1>Hey, Welcome to Login Page!!!</h1>"); 
			 out.print("<form action='' method='post'><p>Please select your position :</p>");
			 out.print("<input type='radio' name='type' value='student'/>Student<br>");
			 out.print("<input type='radio' name='type' value='admin'/>Admin<br>");
			 out.print("<br>Enter username <input type= 'text' name='a1'/>");    
			 out.print("<br>Enter password <input type= 'password' name='a2'/>");     
			 out.print("<br> <input type= 'SUBMIT'  value='LOGIN'/>");   
			 
			 String selectedvalue  =  request.getParameter("type");
			 String name = request.getParameter("a1"); 
			 String pass = request.getParameter("a2"); 
			 
			 if(selectedvalue.equalsIgnoreCase("admin")) {
				 if(StudentDAO.checkUser(name,pass,selectedvalue)) {
						out.print("Welcome "+name);
				 }
					else {
						out.print("Admin not found");
					}
				 }
				 else if(selectedvalue.equalsIgnoreCase("student")) {
					 if(StudentDAO.checkUser(name,pass,selectedvalue)) {
							out.print("Welcome "+name);
						}
						else {
							out.print("Student not found");
						}
				 }
			 out.print("</form></body></center></html>");
			}  
		 catch(Exception e) { 
			 out.print("lllllNot found"+e);
		 }
		
	}

}
