package com.example.demo;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {
	Controller o;
    Student ob;
    List<Student> list;
    @Before
    public void init() {
        o= new Controller();
        ob=new Student();
        list=new ArrayList<>();
        list.add(new Student("Kanu",22,"Jalandhar"));
        list.add(new Student("Shubham",22,"Patiala"));
        list.add(new Student("zayn",25,"Delhi"));
        list.add(new Student("Sidhant",20,"Jammu"));
        list.add(new Student("Samson",21,"Mumbai"));
    }
    
    @Test
    public void test1() {    
        final String url_employees = "http://localhost:8099/hello";
        RestTemplate resttemp= new RestTemplate();
        String result= resttemp.getForObject(url_employees, String.class);
        assertEquals("Welcome to Microservice", result);
    }
    

    @Test
    public void test2() {
        final String URL_EMPLOYEES = "http://localhost:8099/student";
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Student[]> entity = new HttpEntity<Student[]>(headers);// RestTemplate
        RestTemplate restTemplate = new RestTemplate(); // Send request with GET method, and Headers.
        ResponseEntity<Student[]> response = restTemplate.exchange(URL_EMPLOYEES,HttpMethod.GET, entity, Student[].class);
        Student[] result = response.getBody();
        List<Student> asresult=Arrays.asList(result);
        asresult.stream().forEach(System.out::println);
        list.stream().forEach(System.out::println);
        assertEquals(list.size(),asresult.size());
        
    }
}
