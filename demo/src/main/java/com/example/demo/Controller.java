package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	@Autowired
	StudentDAO ob;
	
	@RequestMapping("/hello")
	public String display() {
		return "Welcome to Microservice";
	}
	
	@RequestMapping("/hello1")
	public Student display1() {
		return new Student("Sam",23,"Innocent");
	}
	
	@RequestMapping("/student")
	public List<Student> getStudents(){
		return ob.getStudents();
	}
	
	@RequestMapping("/student/{name}")
	public List<Student> getStudent(@PathVariable String name) {
		return ob.getDetails(name);
	}
	
	@RequestMapping(method=RequestMethod.POST, value = "/AddStudent")
	public String insert(Student bean) {
		return ob.insert(bean);
	}
	
	@RequestMapping(method=RequestMethod.POST, value = "/DeleteStudent")
	public String delete(String name) {
		return ob.delete(name);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/UpdateStudent")
	public String update(String name, Student bean) {
		return ob.update(name, bean);
	}
	
}
