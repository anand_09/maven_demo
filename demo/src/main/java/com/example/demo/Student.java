package com.example.demo;

public class Student {
	private String name;
	private int age;
	private String data;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + ", data=" + data + "]";
	}
	public Student(String name, int age, String data) {
		super();
		this.name = name;
		this.age = age;
		this.data = data;
	}
	public Student() {
		super();
	}
	
}
