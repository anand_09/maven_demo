package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class StudentList {
	private List<Student> list;
	
	public StudentList() {
		super();
		list = new ArrayList<>();
		list.add(new Student("Sam",23,"Master"));
		list.add(new Student("Lucy",21,"Titanic"));
		list.add(new Student("lucy",21,"Titanic"));
		list.add(new Student("Lucy",25,"Titanic"));
		list.add(new Student("Peter",22,"Spiderman"));
	}

	public List<Student> getList() {
		return list;
	}

	public void setList(List<Student> list) {
		this.list = list;
	}
	
}
