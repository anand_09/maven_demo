package com.example.demo;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentDAO {
	
	@Autowired
	StudentList l1;
	public List<Student> getStudents(){
		return l1.getList();
	}
	
	public List<Student> getDetails(String name) {
		return l1.getList().stream().filter(e->e.getName().equals(name)).collect(Collectors.toList());
	}
	
	public String insert(Student ob) {
		l1.getList().add(ob);
		return "Added Successfully";
	}
	
	public String delete(String name) {
		l1.getList().removeIf(e->e.getName().equals(name));
		return "Deleted Successfully";
	}
	
	public String update(String name, Student bean) {
		l1.getList().removeIf(e->e.getName().equals(name));
		l1.getList().add(bean);
		return "Updated Successfully";
	}
	
}
